package homework.chegg.com.chegghomework.model.network

import homework.chegg.com.chegghomework.application.Consts.DATA_SOURCE_A_URL
import homework.chegg.com.chegghomework.application.Consts.DATA_SOURCE_B_URL
import homework.chegg.com.chegghomework.application.Consts.DATA_SOURCE_C_URL
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface NetworkApi {

    @GET(DATA_SOURCE_A_URL)
    fun getSourceA(): Single<Response<SourceAResponse>>

    @GET(DATA_SOURCE_B_URL)
    fun getSourceB(): Single<Response<SourceBResponse>>

    @GET(DATA_SOURCE_C_URL)
    fun getSourceC(): Single<Response<List<SourceCModelResponse>>>

}