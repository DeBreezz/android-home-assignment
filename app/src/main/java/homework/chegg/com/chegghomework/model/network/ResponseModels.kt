package homework.chegg.com.chegghomework.model.network

import com.google.gson.annotations.SerializedName
data class NetworkException(override val message: String) : Exception(message)

data class SourceAResponse(
        @SerializedName("stories") val stories: List<SourceAModelResponse>
)

data class SourceAModelResponse(
        @SerializedName("title") val title: String,
        @SerializedName("subtitle") val subtitle: String,
        @SerializedName("imageUrl") val imageUrl: String
)

data class SourceBResponse(
        @SerializedName("metadata") val metadata: SourceBMetaDataModelResponse
)

data class SourceBMetaDataModelResponse(
        @SerializedName("this") val notImportant: String,
        @SerializedName("innerdata") val innerData: List<SourceBInnerDataModelResponse>
)

data class SourceBInnerDataModelResponse(
        @SerializedName("aticleId") val notImportant: Int,
        @SerializedName("articlewrapper") val article: ArticleWrapperResponse, //think about which name give to this model
        @SerializedName("picture") val picture: String
)

data class ArticleWrapperResponse(
        @SerializedName("header") val header: String,
        @SerializedName("description") val description: String
)

data class SourceCModelResponse(
        @SerializedName("topLine") val topLine: String,
        @SerializedName("subLine1") val subLine1: String,
        @SerializedName("subline2") val subline2: String,
        @SerializedName("image") val image: String
)