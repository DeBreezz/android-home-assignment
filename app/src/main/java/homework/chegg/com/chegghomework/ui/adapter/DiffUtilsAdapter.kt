package homework.chegg.com.chegghomework.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class DiffUtilsAdapter<DATA, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    protected val data = mutableListOf<DATA>()

    fun update(list: List<DATA>) {
        calculateDiffResult(list).run {
            updateData(list)
            dispatchUpdatesTo(this@DiffUtilsAdapter)
        }
    }

    private fun updateData(list: List<DATA>) {
        data.run {
            clear()
            addAll(list)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    private fun calculateDiffResult(list: List<DATA>) =
            DiffUtil.calculateDiff(createDiffCallback(data, list))

    protected open fun createDiffCallback(old: List<DATA>, new: List<DATA>): DiffUtil.Callback {
        return DiffUtilCallback(old, new)
    }
}