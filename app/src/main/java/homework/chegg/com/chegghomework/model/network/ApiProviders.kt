package homework.chegg.com.chegghomework.model.network

import android.util.Log
import homework.chegg.com.chegghomework.common.Mapper
import homework.chegg.com.chegghomework.model.database.SourceAModel
import homework.chegg.com.chegghomework.model.database.SourceBModel
import homework.chegg.com.chegghomework.model.database.SourceCModel
import io.reactivex.Single
import retrofit2.Response

interface NetworkProvider<T> {
    fun get(): Single<List<T>>
}

abstract class BaseNetworkProvider<E, T>(private val mapper: Mapper<E, T>) : NetworkProvider<T> {

    @Throws(NetworkException::class)
    protected fun <T> parseResponse(response: Response<T>): T? {
        if (response.isSuccessful) {
            return response.body()
        }

        throw generateNetworkError()
    }

    private fun generateNetworkError(): Throwable {
        return NetworkException("Generated Network Error Something went wrong")
    }

    protected fun parseToModel(response: List<E>) =
            response.map { source -> toModel(source) }

    private fun toModel(source: E) =
            mapper.mapFrom(source)

}

@Suppress("PrivatePropertyName")
class SourceARemoteProvider(private val networkApi: NetworkApi,
                            mapper: Mapper<SourceAModelResponse, SourceAModel>
) : BaseNetworkProvider<SourceAModelResponse, SourceAModel>(mapper) {

    private val TAG = "SourceARemoteProvider"

    override fun get() = getSource()
            .map { parseResponse(it) }
            .map { parseToModel(it.stories) }

    private fun getSource(): Single<Response<SourceAResponse>> {
        return networkApi
                .getSourceA()
                .doOnSuccess { Log.d(TAG, "getFromRemote getSourceA success data is $it") }
    }
}

@Suppress("PrivatePropertyName")
class SourceBRemoteProvider(private val networkApi: NetworkApi,
                            mapper: Mapper<SourceBInnerDataModelResponse, SourceBModel>) :
        BaseNetworkProvider<SourceBInnerDataModelResponse, SourceBModel>(mapper) {
    private val TAG = "SourceBRemoteProvider"

    override fun get(): Single<List<SourceBModel>> {
        return getSource()
                .map { parseResponse(it) }
                .map { it.metadata.innerData }
                .map { parseToModel(it) }
    }

    private fun getSource(): Single<Response<SourceBResponse>> {
        return networkApi
                .getSourceB()
                .doOnSuccess { Log.d(TAG, "getFromRemote getSourceB success data is $it") }
    }
}

@Suppress("PrivatePropertyName")
class SourceCRemoteProvider(private val networkApi: NetworkApi,
                            mapper: Mapper<SourceCModelResponse, SourceCModel>) :
        BaseNetworkProvider<SourceCModelResponse, SourceCModel>(mapper) {
    private val TAG = "SourceCRemoteProvider"

    override fun get(): Single<List<SourceCModel>> {
        return getSource()
                .map { parseResponse(it) }
                .map { parseToModel(it) }

    }

    private fun getSource(): Single<Response<List<SourceCModelResponse>>> {
        return networkApi
                .getSourceC()
                .doOnSuccess { Log.d(TAG, "getFromRemote getSourceB success data is $it") }
    }
}
