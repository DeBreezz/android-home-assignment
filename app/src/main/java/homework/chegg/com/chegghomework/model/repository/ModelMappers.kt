package homework.chegg.com.chegghomework.model.repository

import homework.chegg.com.chegghomework.common.Mapper
import homework.chegg.com.chegghomework.model.database.SourceAModel
import homework.chegg.com.chegghomework.model.database.SourceBModel
import homework.chegg.com.chegghomework.model.database.SourceCModel

class SourceAMapper : Mapper<List<SourceAModel>, List<SourceModel>> {
    override fun mapFrom(source: List<SourceAModel>): List<SourceModel> {
        return source.map { SourceModel(it.title, it.subtitle, it.imageUrl) }
    }
}

class SourceBMapper : Mapper<List<SourceBModel>, List<SourceModel>> {
    override fun mapFrom(source: List<SourceBModel>): List<SourceModel> {
        return source.map { SourceModel(it.title, it.subtitle, it.imageUrl) }
    }
}

class SourceCMapper : Mapper<List<SourceCModel>, List<SourceModel>> {
    override fun mapFrom(source: List<SourceCModel>): List<SourceModel> {
        return source.map { SourceModel(it.title, it.subtitle, it.imageUrl) }
    }
}
