package homework.chegg.com.chegghomework.model.repository

data class SourceModel(
        val title: String,
        val subTitle: String,
        val imageUrl: String
)