package homework.chegg.com.chegghomework.application

import android.app.Application

@Suppress("unused")
class CheggHomeWorkApp : Application() {

    private val koinStarter = KoinStarter()

    override fun onCreate() {
        super.onCreate()
        intKoin(this)
    }

    private fun intKoin(application: Application) {
        koinStarter.start(application)
    }
}