package homework.chegg.com.chegghomework.model.dataSource

import android.util.Log
import homework.chegg.com.chegghomework.model.cache.CacheProvider
import homework.chegg.com.chegghomework.model.cache.CacheState
import homework.chegg.com.chegghomework.model.network.NetworkProvider
import io.reactivex.Single

private const val TAG = "DataSourceImpl"

class DataSourceImpl<DATA>(private val cacheProvider: CacheProvider<DATA>,
                           private val networkProvider: NetworkProvider<DATA>) : DataSource<List<DATA>> {

    override fun getData(): Single<List<DATA>> {
        return cacheProvider
                .get()
                .flatMap { onCacheReceive(it) }
    }

    private fun getFromRemote(): Single<List<DATA>> {
        return networkProvider.get()
                .flatMap { putToCache(it) }
    }

    private fun putToCache(model: List<DATA>) =
            cacheProvider.put(model)
                    .andThen(Single.just(model))

    private fun onCacheReceive(cacheState: CacheState<DATA>): Single<List<DATA>> {
        return when (cacheState) {
            is CacheState.Actual<DATA> -> getCachedData(cacheState)
            is CacheState.Empty -> getFromRemote()
        }
    }

    private fun getCachedData(cacheState: CacheState.Actual<DATA>): Single<List<DATA>> {
        Log.d(TAG, "data fetched from cache")
        return Single.just(cacheState.data)
    }
}