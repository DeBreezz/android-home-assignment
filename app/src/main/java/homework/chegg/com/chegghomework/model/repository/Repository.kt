package homework.chegg.com.chegghomework.model.repository

import homework.chegg.com.chegghomework.common.Mapper
import homework.chegg.com.chegghomework.model.dataSource.DataSource
import homework.chegg.com.chegghomework.model.database.SourceAModel
import homework.chegg.com.chegghomework.model.database.SourceBModel
import homework.chegg.com.chegghomework.model.database.SourceCModel
import io.reactivex.Single
import io.reactivex.functions.Function3

interface Repository<T> {
    fun getData(): Single<T>
}

class SourcesRepository(private val sourceAds: DataSource<List<SourceAModel>>,
                        private val sourceBds: DataSource<List<SourceBModel>>,
                        private val sourceCds: DataSource<List<SourceCModel>>,
                        private val mapperA: Mapper<List<SourceAModel>, List<SourceModel>>,
                        private val mapperB: Mapper<List<SourceBModel>, List<SourceModel>>,
                        private val mapperC: Mapper<List<SourceCModel>, List<SourceModel>>) : Repository<List<SourceModel>> {
    override fun getData(): Single<List<SourceModel>> {
        return fetchData()
    }

    private fun fetchData(): Single<List<SourceModel>> {
        return Single
                .zip(getSourceA(),
                        getSourceB(),
                        getSourceC(),
                        createABCModel())
    }

    private fun createABCModel(): Function3<List<SourceAModel>, List<SourceBModel>, List<SourceCModel>, List<SourceModel>> {
        return Function3 { a, b, c ->
            mapperA.mapFrom(a) + mapperB.mapFrom(b) + mapperC.mapFrom(c)
        }
    }

    private fun getSourceA() = sourceAds
            .getData()

    private fun getSourceB() = sourceBds
            .getData()

    private fun getSourceC() = sourceCds
            .getData()
}