package homework.chegg.com.chegghomework.model.cache

import io.reactivex.Completable
import io.reactivex.Single

interface CacheProvider<T> {
    fun get(): Single<CacheState<T>>
    fun put(data: List<T>): Completable
}

sealed class CacheState<T> {
    data class Actual<T>(val data: List<T>) : CacheState<T>()
    class Empty<T> : CacheState<T>()
}