package homework.chegg.com.chegghomework.ui

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.ui.adapter.DataRecyclerAdapter
import kotlinx.android.synthetic.main.activity_hw_test.*
import kotlinx.android.synthetic.main.top_bar.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val dataViewModel: DataViewModel by viewModel()
    private val recyclerAdapter = DataRecyclerAdapter()
    private var refreshVector: AnimatedVectorDrawableCompat? = null
    private var loadingVector: AnimatedVectorDrawableCompat? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buildUI()
        initViewModel()
    }

    private fun initButtons() {
        setClickOnRefreshBtn(true)
    }

    private fun buildUI() {
        setContentView(R.layout.activity_hw_test)
        initButtons()
        initViews()
        setRecyclerView()
    }

    private fun initViews() {
        refreshVector = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_refresh)
        loadingVector = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_loop)
        refresh_btn.setImageDrawable(refreshVector)
    }


    private fun setRecyclerView() {
        my_recycler_view.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = this@MainActivity.recyclerAdapter
        }

    }

    private fun initViewModel() {
        dataViewModel.progressLiveData.observe(this, Observer {
            setLoading(it)
        })

        dataViewModel.uiModelLiveData.observe(this, Observer {
            recyclerAdapter.update(it)
        })

        dataViewModel.onErrorLiveData.observe(this, Observer {
            Toast.makeText(this, "Something went wrong plz try again", Toast.LENGTH_SHORT).show()
        })
    }

    private fun setLoading(isLoading: Boolean) {
        if (isLoading) {
            animateLoading()
        } else {
            stopAnimateLoading()
        }
    }

    private fun animateLoading() {
        refresh_btn.setImageDrawable(loadingVector)
        loadingVector?.registerAnimationCallback(animationCallback(
                onEnd = { loadingVector?.start() }))
        loadingVector?.start()
    }

    private fun stopAnimateLoading() {
        loadingVector?.clearAnimationCallbacks()
        setClickOnRefreshBtn(true)
    }

    private fun onButtonRefreshClicked() {
        animateBtn()
        setClickOnRefreshBtn(false)
        onRefreshData()
    }

    private fun setClickOnRefreshBtn(isClick: Boolean) {
        refresh_btn.setOnClickListener { if (isClick) onButtonRefreshClicked() }
    }

    private fun animateBtn() {
        refreshVector?.start()
    }

    private fun onRefreshData() {
        dataViewModel.onFetchDataClicked()
    }
}

private fun animationCallback(
        onEnd: (() -> Unit)? = null,
        onStart: (() -> Unit)? = null)
        : Animatable2Compat.AnimationCallback {
    return object : Animatable2Compat.AnimationCallback() {
        override fun onAnimationEnd(drawable: Drawable?) {
            onEnd?.invoke()
        }

        override fun onAnimationStart(drawable: Drawable?) {
            onStart?.invoke()
        }
    }

}
