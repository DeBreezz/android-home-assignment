@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di

import homework.chegg.com.chegghomework.application.Consts.BASE_URL
import homework.chegg.com.chegghomework.application.Consts.DEFAULT_TIMEOUT
import homework.chegg.com.chegghomework.model.network.NetworkApi
import okhttp3.OkHttpClient
import org.koin.core.scope.Scope
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val retrofitModule = module {

    factory<String> { BASE_URL }

    factory<Converter.Factory> { GsonConverterFactory.create() }

    factory<CallAdapter.Factory> { RxJava2CallAdapterFactory.create() }

    factory<OkHttpClient> { getOkHttp() }

    factory<Retrofit> { getRetrofit() }

    single<NetworkApi> { get<Retrofit>().create(NetworkApi::class.java) }
}

private fun Scope.getRetrofit(): Retrofit {
    return Retrofit.Builder()
            .baseUrl(get<String>())
            .client(get<OkHttpClient>())
            .addCallAdapterFactory(get<CallAdapter.Factory>())
            .addConverterFactory(get<Converter.Factory>())
            .build()
}

private fun getOkHttp(): OkHttpClient {
    return OkHttpClient
            .Builder()
            .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .build()
}