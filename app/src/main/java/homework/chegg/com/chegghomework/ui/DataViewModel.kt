package homework.chegg.com.chegghomework.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import homework.chegg.com.chegghomework.common.LiveDataSingle
import homework.chegg.com.chegghomework.model.repository.Repository
import homework.chegg.com.chegghomework.model.repository.SourceModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

private const val TAG = "DataViewModel"

class DataViewModel(private val repository: Repository<List<SourceModel>>) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val uiModelMutableLiveData = MutableLiveData<List<SourceModel>>()
    val uiModelLiveData: LiveData<List<SourceModel>> = uiModelMutableLiveData

    private val progressMutableLiveData = MutableLiveData<Boolean>()
    val progressLiveData: LiveData<Boolean> = progressMutableLiveData

    private val onErrorMutableLiveData = LiveDataSingle<Unit>()
    val onErrorLiveData: LiveData<Unit> = onErrorMutableLiveData

    fun onFetchDataClicked() {
        repository
                .getData()
                .doOnSuccess { Log.d(TAG, "onFetchDataClicked getData() is [$it]") }
                .doOnSubscribe { setProgress(true) }
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                        onSuccess = { onDataReceived(it) },
                        onError = { onErrorReceive(it) }
                ).addTo(disposables)
    }

    private fun onDataReceived(uiModel: List<SourceModel>) {
        setProgress(false)
        uiModelMutableLiveData.postValue(uiModel)
    }

    private fun onErrorReceive(throwable: Throwable) {
        Log.e(TAG, "Error during fetching data $throwable")
        setProgress(false)
        onErrorMutableLiveData.postValue(Unit)
    }

    private fun setProgress(inProgress: Boolean) {
        progressMutableLiveData.postValue(inProgress)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}