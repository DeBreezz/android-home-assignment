package homework.chegg.com.chegghomework.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface SourceDao<SOURCE> {

    fun getAll(): Single<List<SOURCE>>

    @Insert
    fun insertAll(source: List<SOURCE>): Completable

    fun deleteAll(): Completable

    @Query("SELECT timeStamp FROM source_timestamp WHERE source_type Like :type")
    fun getTimestamp(type: String): Maybe<Long>

    @Insert
    fun setTimestamp(timestampModel: SourcesTimeStamp): Completable

    @Query("DELETE FROM source_timestamp WHERE source_type Like :type")
    fun delTimestamp(type: String): Completable
}

@Dao
interface SourceADao : SourceDao<SourceAModel> {

    @Query("SELECT * FROM source_a")
    override fun getAll(): Single<List<SourceAModel>>

    @Query("DELETE FROM source_a")
    override fun deleteAll(): Completable
}

@Dao
interface SourceBDao : SourceDao<SourceBModel> {

    @Query("SELECT * FROM source_b")
    override fun getAll(): Single<List<SourceBModel>>

    @Query("DELETE FROM source_b")
    override fun deleteAll(): Completable

}

@Dao
interface SourceCDao : SourceDao<SourceCModel> {

    @Query("SELECT * FROM source_c")
    override fun getAll(): Single<List<SourceCModel>>

    @Query("DELETE FROM source_c")
    override fun deleteAll(): Completable

}