package homework.chegg.com.chegghomework.model.cache

import android.util.Log
import homework.chegg.com.chegghomework.model.database.SourceDao
import homework.chegg.com.chegghomework.model.database.SourceType
import homework.chegg.com.chegghomework.model.database.SourcesTimeStamp
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class DataBaseCacheProvider<T>(private val timeoutPolicy: TimeoutCachePolicy,
                               private val type: SourceType,
                               private val dbSource: SourceDao<T>) : CacheProvider<T> {
    @Suppress("PrivatePropertyName")
    private val TAG: String = "DataBaseCacheProvider $type"
    private val disposables = CompositeDisposable()

    init {
        dbSource
                .getTimestamp(type.name)
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                        onSuccess = { onTimeStampReceive(it) },
                        onComplete = { onEmptyData() },
                        onError = { onError(it) }
                ).addTo(disposables)
    }

    override fun get(): Single<CacheState<T>> {
        return getCachedData()
    }

    private fun onTimeStampReceive(timeStamp: Long) {
        Log.e(TAG, "onTimeStampReceive timestamp is $timeStamp")
        timeoutPolicy.setTimeStamp(timeStamp)
        disposables.clear()
    }

    private fun onEmptyData() {
        Log.e(TAG, "onEmptyData")
        disposables.clear()
    }

    private fun onError(throwable: Throwable) {
        Log.e(TAG, "Error during fetching time stamp", throwable)
        disposables.clear()
    }

    override fun put(data: List<T>): Completable {
        return clearData()
                .andThen(saveData(data))
                .andThen(innerSaveTimeStamp())
                .subscribeOn(Schedulers.io())
                .doOnComplete { Log.d(TAG, "put() data to the db completed") }
                .doOnError { Log.e(TAG, "put() Error during inserting data to db", it) }
    }

    private fun clearData(): Completable {
        return dbSource
                .delTimestamp(type.name)
                .andThen(dbSource.deleteAll())
    }

    private fun saveData(data: List<T>): Completable {
        return dbSource.insertAll(data)
    }

    private fun innerSaveTimeStamp(): Completable {
        timeoutPolicy.setTimeStamp(System.currentTimeMillis())
        return dbSource.setTimestamp(SourcesTimeStamp(timeStamp = timeoutPolicy.getTimeStamp(), type = type.name))
    }

    private fun getCachedData(): Single<CacheState<T>> {
        return when {
            timeoutPolicy.isValid() -> getFromDb()
            else -> returnEmptyState()
        }
    }

    private fun getFromDb(): Single<CacheState<T>> {
        return dbSource
                .getAll()
                .flatMap { Single.just(CacheState.Actual(it)) }
    }

    private fun returnEmptyState(): Single<CacheState<T>> {
        return clearData()
                .andThen(generateEmptyState())
    }

    private fun generateEmptyState() =
            Single.just(CacheState.Empty<T>() as CacheState<T>)

}


