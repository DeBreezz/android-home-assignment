package homework.chegg.com.chegghomework.model.cache

import android.util.Log
import io.reactivex.Completable
import io.reactivex.Single


private const val TAG = "InMemoryCacheProvider"

class InMemoryCacheProvider<T>(private val timeoutPolicy: TimeoutCachePolicy) : CacheProvider<T> {

    private var list = mutableListOf<T>()

    override fun get(): Single<CacheState<T>> {
        return getCachedData()
    }

    override fun put(data: List<T>): Completable {
        return clearData()
                .andThen(saveToCache(data))

    }

    private fun saveToCache(data: List<T>): Completable {
        timeoutPolicy.setTimeStamp(System.currentTimeMillis())
        return Completable.fromAction { list.addAll(data) }
                .doOnComplete { Log.d(TAG, "put() data to the memory completed") }
                .doOnError { Log.e(TAG, "put() Error during inserting data to memory", it) }
    }

    private fun getCachedData(): Single<CacheState<T>> {
        return when {
            timeoutPolicy.isValid() -> getFromCache()
            else -> returnEmptyState()
        }
    }

    private fun getFromCache(): Single<CacheState<T>> {
        Log.d(TAG, "getFromCache() fetching data from in memory cache completed")
        return Single.just(CacheState.Actual(list))
    }

    private fun returnEmptyState(): Single<CacheState<T>> {
        return clearData()
                .andThen(generateEmptyState())
    }

    private fun generateEmptyState() =
            Single.just(CacheState.Empty<T>() as CacheState<T>)


    private fun clearData(): Completable {
        return Completable.fromAction {
            release()
        }
    }

    private fun release() {
        list.clear()
    }
}