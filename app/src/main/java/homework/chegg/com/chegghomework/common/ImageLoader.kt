package homework.chegg.com.chegghomework.common

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import homework.chegg.com.chegghomework.R

object ImageLoader {

    fun loadImage(url: String, imageView: ImageView) {
        Glide
                .with(imageView)
                .setDefaultRequestOptions(getRequestOptions())
                .load(url)
                .into(imageView)
    }

    private fun getRequestOptions() =
            RequestOptions().apply {
                placeholder(R.drawable.loading_ph)
                error(R.drawable.placeholder)
            }
}
