@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di

import androidx.room.Room
import homework.chegg.com.chegghomework.model.database.AppDataBase
import homework.chegg.com.chegghomework.model.database.SourceADao
import homework.chegg.com.chegghomework.model.database.SourceBDao
import homework.chegg.com.chegghomework.model.database.SourceCDao
import org.koin.android.ext.koin.androidContext
import org.koin.core.scope.Scope
import org.koin.dsl.module

val dbModule = module {

    single<AppDataBase> { createDBInstance() }
    single<SourceADao> { get<AppDataBase>().sourceA() }
    single<SourceBDao> { get<AppDataBase>().sourceB() }
    single<SourceCDao> { get<AppDataBase>().sourceC() }
}

private fun Scope.createDBInstance() =
        Room.databaseBuilder(androidContext(), AppDataBase::class.java, "chegg_hw").build()