@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di

import homework.chegg.com.chegghomework.application.Consts.DEFAULT_CACHE_A_TIMEOUT
import homework.chegg.com.chegghomework.application.Consts.DEFAULT_CACHE_B_TIMEOUT
import homework.chegg.com.chegghomework.application.Consts.DEFAULT_CACHE_C_TIMEOUT
import homework.chegg.com.chegghomework.model.cache.DataBaseCacheProvider
import homework.chegg.com.chegghomework.model.cache.InMemoryCacheProvider
import homework.chegg.com.chegghomework.model.cache.TimeoutCachePolicyImpl
import homework.chegg.com.chegghomework.model.dataSource.DataSourceImpl
import homework.chegg.com.chegghomework.model.dataSource.mapper.SourceAResponseMapper
import homework.chegg.com.chegghomework.model.dataSource.mapper.SourceBResponseMapper
import homework.chegg.com.chegghomework.model.dataSource.mapper.SourceCResponseMapper
import homework.chegg.com.chegghomework.model.database.*
import homework.chegg.com.chegghomework.model.network.NetworkApi
import homework.chegg.com.chegghomework.model.network.SourceARemoteProvider
import homework.chegg.com.chegghomework.model.network.SourceBRemoteProvider
import homework.chegg.com.chegghomework.model.network.SourceCRemoteProvider
import homework.chegg.com.chegghomework.model.repository.SourceAMapper
import homework.chegg.com.chegghomework.model.repository.SourceBMapper
import homework.chegg.com.chegghomework.model.repository.SourceCMapper
import homework.chegg.com.chegghomework.model.repository.SourcesRepository
import homework.chegg.com.chegghomework.ui.DataViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

private const val POLICY_A = "CacheAPolicy"
private const val POLICY_B = "CacheBPolicy"
private const val POLICY_C = "CacheCPolicy"
private const val CACHE_A = "CacheA"
private const val CACHE_B = "CacheB"
private const val CACHE_C = "CacheC"
private const val DATA_SOURCE_A = "SourceADataSource"
private const val DATA_SOURCE_B = "SourceBDataSource"
private const val DATA_SOURCE_C = "SourceCDataSource"

val abcDataModule = module {

    single<SourceAResponseMapper> { SourceAResponseMapper() }
    single<SourceBResponseMapper> { SourceBResponseMapper() }
    single<SourceCResponseMapper> { SourceCResponseMapper() }
    single<SourceAMapper> { SourceAMapper() }
    single<SourceBMapper> { SourceBMapper() }
    single<SourceCMapper> { SourceCMapper() }

    factory(named(POLICY_A)) { TimeoutCachePolicyImpl(DEFAULT_CACHE_A_TIMEOUT) }
    factory(named(POLICY_B)) { TimeoutCachePolicyImpl(DEFAULT_CACHE_B_TIMEOUT) }
    factory(named(POLICY_C)) { TimeoutCachePolicyImpl(DEFAULT_CACHE_C_TIMEOUT) }

    factory(named(CACHE_A)) { InMemoryCacheProvider<SourceAModel>(get(named(POLICY_A))) }
    factory(named(CACHE_B)) { DataBaseCacheProvider<SourceBModel>(get(named(POLICY_B)), SourceType.B, get<SourceBDao>()) }
    factory(named(CACHE_C)) { DataBaseCacheProvider<SourceCModel>(get(named(POLICY_C)), SourceType.C, get<SourceCDao>()) }

    factory<SourceARemoteProvider> { SourceARemoteProvider(get<NetworkApi>(), get<SourceAResponseMapper>()) }
    factory<SourceBRemoteProvider> { SourceBRemoteProvider(get<NetworkApi>(), get<SourceBResponseMapper>()) }
    factory<SourceCRemoteProvider> { SourceCRemoteProvider(get<NetworkApi>(), get<SourceCResponseMapper>()) }

    factory(named(DATA_SOURCE_A)) { DataSourceImpl(get(named(CACHE_A)), get<SourceARemoteProvider>()) }
    factory(named(DATA_SOURCE_B)) { DataSourceImpl(get(named(CACHE_B)), get<SourceBRemoteProvider>()) }
    factory(named(DATA_SOURCE_C)) { DataSourceImpl(get(named(CACHE_C)), get<SourceCRemoteProvider>()) }

    factory<SourcesRepository> {
        SourcesRepository(get(named(DATA_SOURCE_A)),
                get(named(DATA_SOURCE_B)),
                get(named(DATA_SOURCE_C)),
                get<SourceAMapper>(),
                get<SourceBMapper>(),
                get<SourceCMapper>()
        )
    }

    viewModel { DataViewModel(get<SourcesRepository>()) }
}