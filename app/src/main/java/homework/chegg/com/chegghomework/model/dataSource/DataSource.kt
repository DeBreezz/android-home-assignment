package homework.chegg.com.chegghomework.model.dataSource

import io.reactivex.Single

interface DataSource<T> {
    fun getData(): Single<T>
}
