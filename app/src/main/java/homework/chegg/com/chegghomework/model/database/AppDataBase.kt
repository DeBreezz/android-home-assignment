package homework.chegg.com.chegghomework.model.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
        entities = [SourceAModel::class,
            SourceBModel::class,
            SourceCModel::class,
            SourcesTimeStamp::class],
        version = 1
)
abstract class AppDataBase : RoomDatabase() {
    abstract fun sourceA(): SourceADao
    abstract fun sourceB(): SourceBDao
    abstract fun sourceC(): SourceCDao
}