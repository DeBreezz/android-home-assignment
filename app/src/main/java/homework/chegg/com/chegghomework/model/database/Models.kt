package homework.chegg.com.chegghomework.model.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import homework.chegg.com.chegghomework.model.network.SourceAModelResponse
import homework.chegg.com.chegghomework.model.network.SourceBInnerDataModelResponse
import homework.chegg.com.chegghomework.model.network.SourceCModelResponse

@Entity(tableName = "source_a")
data class SourceAModel(
        @PrimaryKey(autoGenerate = true) var id: Int = 0,
        @ColumnInfo(name = "source_a_title") val title: String,
        @ColumnInfo(name = "source_a_subtitle") val subtitle: String,
        @ColumnInfo(name = "source_a_image_url") val imageUrl: String
)

@Entity(tableName = "source_b")
data class SourceBModel(
        @PrimaryKey(autoGenerate = true) var id: Int = 0,
        @ColumnInfo(name = "source_b_title") val title: String,
        @ColumnInfo(name = "source_b_subtitle") val subtitle: String,
        @ColumnInfo(name = "source_b_image_url") val imageUrl: String
)

@Entity(tableName = "source_c")
data class SourceCModel(
        @PrimaryKey(autoGenerate = true) var id: Int = 0,
        @ColumnInfo(name = "source_c_title") val title: String,
        @ColumnInfo(name = "source_c_subtitle") val subtitle: String,
        @ColumnInfo(name = "source_c_imageUrl") val imageUrl: String
)

@Entity(tableName = "source_timestamp")
data class SourcesTimeStamp(@PrimaryKey val timeStamp: Long,
                            @ColumnInfo(name = "source_type") val type: String)

enum class SourceType {
    A, B, C
}
