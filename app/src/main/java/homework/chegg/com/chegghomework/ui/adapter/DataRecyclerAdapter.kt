package homework.chegg.com.chegghomework.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.common.ImageLoader
import homework.chegg.com.chegghomework.model.repository.SourceModel
import kotlinx.android.synthetic.main.card_item.view.*

class DataRecyclerAdapter : DiffUtilsAdapter<SourceModel, DataRecyclerAdapter.DataRecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataRecyclerViewHolder {
        val itemView: View =
                LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false)

        return DataRecyclerViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DataRecyclerViewHolder, position: Int) {
        holder.bind(data[position])
    }

    class DataRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(uiModel: SourceModel) {
            bindTitles(uiModel)
            loadImage(uiModel)
        }

        private fun bindTitles(uiModel: SourceModel) {
            itemView.textView_card_item_title.text = uiModel.title
            itemView.textView_card_item_subtitle.text = uiModel.title
        }

        private fun loadImage(uiModel: SourceModel) {
            ImageLoader.loadImage(uiModel.imageUrl, itemView.imageView_card_item)
        }
    }
}