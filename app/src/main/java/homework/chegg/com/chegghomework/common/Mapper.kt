package homework.chegg.com.chegghomework.common

interface Mapper<E, T> {
    fun mapFrom(source: E): T
}