package homework.chegg.com.chegghomework.model.dataSource.mapper

import homework.chegg.com.chegghomework.common.Mapper
import homework.chegg.com.chegghomework.model.database.SourceAModel
import homework.chegg.com.chegghomework.model.database.SourceBModel
import homework.chegg.com.chegghomework.model.database.SourceCModel
import homework.chegg.com.chegghomework.model.network.SourceAModelResponse
import homework.chegg.com.chegghomework.model.network.SourceBInnerDataModelResponse
import homework.chegg.com.chegghomework.model.network.SourceCModelResponse

class SourceAResponseMapper : Mapper<SourceAModelResponse, SourceAModel> {
    override fun mapFrom(source: SourceAModelResponse): SourceAModel {
        return SourceAModel(title = source.title,
                subtitle = source.subtitle,
                imageUrl = source.imageUrl)
    }
}

class SourceBResponseMapper : Mapper<SourceBInnerDataModelResponse, SourceBModel> {
    override fun mapFrom(source: SourceBInnerDataModelResponse): SourceBModel {
        return SourceBModel(title = source.article.header,
                subtitle = source.article.description,
                imageUrl = source.picture
        )
    }
}

class SourceCResponseMapper : Mapper<SourceCModelResponse, SourceCModel> {
    override fun mapFrom(source: SourceCModelResponse): SourceCModel {
        return SourceCModel(title = source.topLine,
                subtitle = "${source.subLine1}${source.subline2}",
                imageUrl = source.image)
    }
}