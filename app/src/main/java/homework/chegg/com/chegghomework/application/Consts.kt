package homework.chegg.com.chegghomework.application

object Consts {

    const val BASE_URL = "http://chegg-mobile-promotioms.s3.amazonaws.com/android/homework/"

    const val DATA_SOURCE_A_URL = "android_homework_datasourceA.json"
    const val DATA_SOURCE_B_URL = "android_homework_datasourceB.json"
    const val DATA_SOURCE_C_URL = "android_homework_datasourceC.json"

    const val DEFAULT_TIMEOUT = 30L

    const val DEFAULT_CACHE_A_TIMEOUT = 5 * 1000 * 60L
    const val DEFAULT_CACHE_B_TIMEOUT = 30 * 1000 * 60L
    const val DEFAULT_CACHE_C_TIMEOUT = 60 * 1000 * 60L

}
