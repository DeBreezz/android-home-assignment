package homework.chegg.com.chegghomework.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import homework.chegg.com.chegghomework.model.network.NetworkException
import homework.chegg.com.chegghomework.model.repository.SourceModel
import homework.chegg.com.chegghomework.model.repository.SourcesRepository
import homework.chegg.com.chegghomework.utils.RxImmediateSchedulerRule
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class DataViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    private val repo = mock(SourcesRepository::class.java)

    private lateinit var dataViewModel: DataViewModel

    @Mock
    lateinit var mockErrorDataObserver: Observer<Unit>
    @Mock
    lateinit var mockUiDataObserver: Observer<List<SourceModel>>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        dataViewModel = DataViewModel(repo)
    }

    @Test
    fun onFetchData_success() {
        val data = generateData()

        `when`(repo.getData()).thenReturn(Single.just(data))

        dataViewModel.onErrorLiveData.observeForever(mockErrorDataObserver)
        dataViewModel.uiModelLiveData.observeForever(mockUiDataObserver)

        dataViewModel.onFetchDataClicked()

        val postedData = dataViewModel.uiModelLiveData.value
        postedData?.let {
            Assert.assertEquals(data.size, it.size)
            it.forEachIndexed { index, uiModel ->
                Assert.assertEquals(data[index], uiModel)
            }
        }

        verify(mockErrorDataObserver, times(0)).onChanged(Unit)
        verify(mockUiDataObserver, times(1))
    }

    @Test
    fun onFetchData_error() {
        val error = NetworkException("test error")
        `when`(repo.getData()).thenReturn(Single.error(error))

        dataViewModel.onErrorLiveData.observeForever(mockErrorDataObserver)
        dataViewModel.uiModelLiveData.observeForever(mockUiDataObserver)

        dataViewModel.onFetchDataClicked()

        verify(mockErrorDataObserver, times(1)).onChanged(Unit)
        verify(mockUiDataObserver, times(0)).onChanged(emptyList())
    }

    private fun generateData() = listOf(SourceModel("chegg", "test", "some_url"),
            SourceModel("chegg1", "test1", "some_url1"),
            SourceModel("chegg2", "test2", "some_url2"),
            SourceModel("chegg3", "test3", "some_url3"))

}