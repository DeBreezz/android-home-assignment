package homework.chegg.com.chegghomework.model.cache

import org.junit.Assert
import org.junit.Test

class TimeoutCachePolicyImplTest {

    private val cachePolicy = TimeoutCachePolicyImpl(10000L)

    @Test
    fun isValid_success() {
        cachePolicy.setTimeStamp(System.currentTimeMillis() - 1000)
        val isValid = cachePolicy.isValid()
        assert(isValid)
    }

    @Test
    fun isValid_fail() {
        cachePolicy.setTimeStamp(System.currentTimeMillis() - 100000)
        val isValid = cachePolicy.isValid()
        assert(!isValid)
    }

    @Test
    fun getTimeStamp_test() {
        val timeStamp = System.currentTimeMillis()
        cachePolicy.setTimeStamp(timeStamp)
        val timeStampFromPolicy = cachePolicy.getTimeStamp()
        Assert.assertEquals(timeStamp, timeStampFromPolicy)
    }
}