package homework.chegg.com.chegghomework.model.dataSource.mapper

import homework.chegg.com.chegghomework.model.network.SourceAModelResponse
import org.junit.Assert.assertEquals
import org.junit.Test

class SourceAResponseMapperTest {

    private val mapper = SourceAResponseMapper()

    @Test
    fun from_test() {
        val mockData = SourceAModelResponse("chegg", "the best", "some_url")

        val newData = mapper.mapFrom(mockData)

        assertEquals(mockData.title, newData.title)
        assertEquals(mockData.subtitle, newData.subtitle)
        assertEquals(mockData.imageUrl, newData.imageUrl)

    }
}