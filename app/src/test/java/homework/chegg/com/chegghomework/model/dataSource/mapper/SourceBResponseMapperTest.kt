package homework.chegg.com.chegghomework.model.dataSource.mapper

import homework.chegg.com.chegghomework.model.network.ArticleWrapperResponse
import homework.chegg.com.chegghomework.model.network.SourceBInnerDataModelResponse
import junit.framework.Assert.assertEquals

import org.junit.Test

class SourceBResponseMapperTest {

    private val mapper = SourceBResponseMapper()

    @Test
    fun mapFrom() {
        val mockData = SourceBInnerDataModelResponse(1, ArticleWrapperResponse("Chegg", "Test"), "some_url")

        val newData = mapper.mapFrom(mockData)

        assertEquals(mockData.article.header, newData.title)
        assertEquals(mockData.article.description, newData.subtitle)
        assertEquals(mockData.picture, newData.imageUrl)
    }
}