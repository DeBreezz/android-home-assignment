package homework.chegg.com.chegghomework.model.repository

import homework.chegg.com.chegghomework.model.dataSource.DataSource
import homework.chegg.com.chegghomework.model.dataSource.DataSourceImpl
import homework.chegg.com.chegghomework.model.database.SourceAModel
import homework.chegg.com.chegghomework.model.database.SourceBModel
import homework.chegg.com.chegghomework.model.database.SourceCModel
import homework.chegg.com.chegghomework.model.network.NetworkException
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations


class SourcesRepositoryTest {

    @Mock
    lateinit var dataSourceA: DataSource<List<SourceAModel>>
    @Mock
    lateinit var dataSourceB: DataSource<List<SourceBModel>>
    @Mock
    lateinit var dataSourceC: DataSource<List<SourceCModel>>

    private val mapperA = mock(SourceAMapper::class.java)
    private val mapperB = mock(SourceBMapper::class.java)
    private val mapperC = mock(SourceCMapper::class.java)

    private lateinit var repository: Repository<List<SourceModel>>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = SourcesRepository(dataSourceA,
                dataSourceB,
                dataSourceC,
                mapperA,
                mapperB,
                mapperC)
    }

    @Test
    fun getData_success() {

        val dataA = generateA()
        val dataB = generateB()
        val dataC = generateC()
        val model = generateModel()

        `when`(dataSourceA.getData()).thenReturn(Single.just(dataA))
        `when`(dataSourceB.getData()).thenReturn(Single.just(dataB))
        `when`(dataSourceC.getData()).thenReturn(Single.just(dataC))

        `when`(mapperA.mapFrom(dataA)).thenReturn(listOf(model[0]))
        `when`(mapperB.mapFrom(dataB)).thenReturn(listOf(model[1]))
        `when`(mapperC.mapFrom(dataC)).thenReturn(listOf(model[2]))

        val result = repository.getData()

        val testObserver = TestObserver<List<SourceModel>>()

        result.subscribe(testObserver)

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val listResult = testObserver.values()[0]
        assertThat(listResult.size, `is`(model.size))

        listResult.forEachIndexed { index, sourceModel ->
            assertEquals(model[index], sourceModel)
        }
        testObserver.dispose()
    }

    @Test
    fun getData_fail() {

        val dataA = generateA()
        val dataB = generateB()
        val dataC = generateC()
        val model = generateModel()
        val error = NetworkException("test error")

        `when`(dataSourceA.getData()).thenReturn(Single.just(dataA))
        `when`(dataSourceB.getData()).thenReturn(Single.error(error))
        `when`(dataSourceC.getData()).thenReturn(Single.just(dataC))

        `when`(mapperA.mapFrom(dataA)).thenReturn(listOf(model[0]))
        `when`(mapperB.mapFrom(dataB)).thenReturn(listOf(model[1]))
        `when`(mapperC.mapFrom(dataC)).thenReturn(listOf(model[2]))

        val result = repository.getData()

        val testObserver = TestObserver<List<SourceModel>>()

        result.subscribe(testObserver)

        testObserver.assertError(error)
        testObserver.dispose()
    }

    private fun generateA() = listOf(SourceAModel(1, "chegg", "test", "some_url"))

    private fun generateB() = listOf(SourceBModel(3, "cheggB", "test", "some_url"))

    private fun generateC() = listOf(SourceCModel(4, "cheggC", "test", "some_url"))

    private fun generateModel() = listOf(SourceModel("chegg", "test", "some_url"),
            SourceModel("cheggB", "test", "some_url"),
            SourceModel("cheggC", "test", "some_url"))
}