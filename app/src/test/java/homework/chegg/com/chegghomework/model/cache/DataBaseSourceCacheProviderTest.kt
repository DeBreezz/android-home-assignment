package homework.chegg.com.chegghomework.model.cache

import homework.chegg.com.chegghomework.model.database.SourceBDao
import homework.chegg.com.chegghomework.model.database.SourceBModel
import homework.chegg.com.chegghomework.model.database.SourceType
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

class DataBaseSourceCacheProviderTest {

    private val dao = mock(SourceBDao::class.java)
    private val type = mock(SourceType::class.java)
    private val policy = mock(TimeoutCachePolicy::class.java)
    private lateinit var provider: DataBaseCacheProvider<SourceBModel>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun get_cached_data() {

        val data = generateData()

        `when`(type.name).thenReturn(SourceType.B.name)
        `when`(dao.getTimestamp(type.name)).thenReturn(Maybe.just(0L))
        `when`(policy.isValid()).thenReturn(true)
        `when`(dao.getAll()).thenReturn(Single.just(data))

        provider = DataBaseCacheProvider(policy, type, dao)

        val subscriber = provider.get()
        val testObserver = TestObserver<CacheState<SourceBModel>>()

        subscriber.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]

        Assert.assertEquals(result is CacheState.Actual, true)

        val dataFromCache = (result as CacheState.Actual).data
        Assert.assertThat(dataFromCache.size, CoreMatchers.`is`(data.size))

        dataFromCache.forEachIndexed { index, sourceBModel ->
            Assert.assertEquals(data[index], sourceBModel)
        }

        testObserver.dispose()
    }


    @Test
    fun get_no_cached() {

        `when`(type.name).thenReturn(SourceType.B.name)
        `when`(dao.getTimestamp(type.name)).thenReturn(Maybe.just(0L))
        `when`(policy.isValid()).thenReturn(false)
        `when`(dao.delTimestamp(type.name)).thenReturn(Completable.complete())
        `when`(dao.deleteAll()).thenReturn(Completable.complete())

        provider = DataBaseCacheProvider(policy, type, dao)

        val subscriber = provider.get()
        val testObserver = TestObserver<CacheState<SourceBModel>>()

        subscriber.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val result = testObserver.values()[0]

        Assert.assertEquals(result is CacheState.Empty, true)
        testObserver.dispose()
    }

    private fun generateData() = listOf(SourceBModel(1, "chegg", "test", "some_url"),
            SourceBModel(2, "chegg1", "test1", "some_url1"),
            SourceBModel(3, "chegg2", "test2", "some_url2"),
            SourceBModel(4, "chegg3", "test3", "some_url3"))
}