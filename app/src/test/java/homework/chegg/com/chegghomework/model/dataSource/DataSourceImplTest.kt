package homework.chegg.com.chegghomework.model.dataSource

import homework.chegg.com.chegghomework.model.cache.CacheProvider
import homework.chegg.com.chegghomework.model.cache.CacheState
import homework.chegg.com.chegghomework.model.database.SourceAModel
import homework.chegg.com.chegghomework.model.network.NetworkException
import homework.chegg.com.chegghomework.model.network.NetworkProvider
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

@Suppress("UNCHECKED_CAST")
class DataSourceImplTest {

    @Mock
    private lateinit var cache: CacheProvider<SourceAModel>
    @Mock
    private lateinit var network: NetworkProvider<SourceAModel>

    private lateinit var dataSource: DataSourceImpl<SourceAModel>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        dataSource = DataSourceImpl(cache, network)
    }

    @Test
    fun getData_from_cache() {

        val data = generateData()
        val cacheActualState = CacheState.Actual(data) as CacheState<SourceAModel>

        `when`(cache.get()).thenReturn(Single.just(cacheActualState))

        val subscriber = dataSource.getData()

        val testObserver = TestObserver<List<SourceAModel>>()

        subscriber.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val listResult = testObserver.values()[0]

        Assert.assertThat(listResult.size, `is`(data.size))

        listResult.forEachIndexed { index, sourceModel ->
            Assert.assertEquals(data[index], sourceModel)
        }
        testObserver.dispose()
    }

    @Test
    fun getData_from_remote_success() {

        val data = generateData()
        val cacheEmptyState = CacheState.Empty<SourceAModel>()

        `when`(cache.get()).thenReturn(Single.just(cacheEmptyState))
        `when`(cache.put(data)).thenReturn(Completable.complete())
        `when`(network.get()).thenReturn(Single.just(data))

        val subscriber = dataSource.getData()

        val testObserver = TestObserver<List<SourceAModel>>()

        subscriber.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val listResult = testObserver.values()[0]

        Assert.assertThat(listResult.size, `is`(data.size))

        listResult.forEachIndexed { index, sourceModel ->
            Assert.assertEquals(data[index], sourceModel)
        }

        testObserver.dispose()
    }

    @Test
    fun getData_from_remote_without_success() {

        val cacheEmptyState = CacheState.Empty<SourceAModel>()
        val error = NetworkException("test error")

        `when`(cache.get()).thenReturn(Single.just(cacheEmptyState))
        `when`(network.get()).thenReturn(Single.error(error))

        val subscriber = dataSource.getData()

        val testObserver = TestObserver<List<SourceAModel>>()

        subscriber.subscribe(testObserver)

        testObserver.assertError(error)

        testObserver.dispose()
    }


    private fun generateData() = listOf(SourceAModel(1, "chegg", "test", "some_url"))
}