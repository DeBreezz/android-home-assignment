package homework.chegg.com.chegghomework.model.dataSource.mapper

import homework.chegg.com.chegghomework.model.network.SourceCModelResponse
import org.junit.Assert.assertEquals
import org.junit.Test

class SourceCResponseMapperTest {

    private val mapper = SourceCResponseMapper()

    @Test
    fun mapFrom() {
        val mockData = SourceCModelResponse("chegg", "te", "st", "some_url")

        val newData = mapper.mapFrom(mockData)

        assertEquals(mockData.topLine, newData.title)
        assertEquals("${mockData.subLine1}${mockData.subline2}", newData.subtitle)
        assertEquals(mockData.image, newData.imageUrl)
    }
}